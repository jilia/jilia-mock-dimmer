var Device = require('zetta-device');
var util = require('util');

// this is a first pass, we can improve the logic/behavior later

// Name defaults to 'dimmer' if not provided
var Dimmer = module.exports = function(name) {
  Device.call(this);
  this.name = name || 'dimmer';

  this._transistionRate = 5.0;
  this.currentLevel = 0.0;
  this.onOff = 'Off';
}

util.inherits(Dimmer, Device);

Dimmer.prototype.init = function(config) {
  config
    .name(this.name)
    .type('light')
    .monitor('currentLevel')
    .monitor('onOff')
    .state('ready')
    .when('ready', {allow:['On','Off','Toggle','ChangeLevel']})
    .map('On', this.turnOn)
    .map('Off', this.turnOff)
    .map('Toggle', this.toggle)
    .map('ChangeLevel', this.setLevel, [{name:'level',type:'number'},{name:'transitionTime',type:'number'}]);
}


Dimmer.prototype._updateLevel = function() {
  var lvlNum = Number(this.currentLevel);
  var temp = lvlNum + this._rate;

  // if we are moving down and we go below our target, bounce back to it
  if(this._rate < 0 && temp < this._targetLevel) {
    temp = this._targetLevel;
    this._rate = this._targetLevel = null;
  } else if(this._rate > 0 && temp > this._targetLevel) {
    // if we go above the target, bounce back
    temp = this._targetLevel;
    this._rate = this._targetLevel = null;
  }

  this.currentLevel = temp.toString();
  if(temp > 0 ) {
      this.onOff = 'On';
    } else {
      this.onOff = 'Off';
    }

  if(this._rate) {
    setTimeout(this._updateLevel.bind(this), 1000);
  }
};

Dimmer.prototype.setLevel = function(level, rate, cb) {
  level = Number(level);
  if(level > 100.0) level = 100.0;
  else if(level < 0.0) level = 0.0;

  if( rate > 0 ) {
    // transition from 'currentLevel' to _targetLevel over rate
    this._rate = (level - Number(this.currentLevel)) / rate;
    this._targetLevel = level;

    // update every second
    setTimeout(this._updateLevel.bind(this), 1000);
  } else {
    this.currentLevel = level.toString();
    if(level > 0 ) {
      this.onOff = 'On';
    } else {
      this.onOff = 'Off';
    }
    this._rate = 0;
  }

  cb(null);
};

Dimmer.prototype.turnOn = function(cb) {
  this.setLevel(100.0, 0, cb);
};


Dimmer.prototype.turnOff = function(cb) {
  this.setLevel(0.0, 0, cb);
}

Dimmer.prototype.toggle = function(cb) {
  var level = 100.0;
  if(Number(this.currentLevel) > 0) {
    level = 0.0;
  }

  this.setLevel(level, 0, cb);
};